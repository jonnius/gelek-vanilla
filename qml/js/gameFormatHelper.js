function startGameByFormat(filePath, gameFormat) {
    switch (gameFormat.replace(" (non-authoritative)", "")) {
        case "advsys":
            mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "advsysremglk", "game": filePath, "pageTitle": i18n.tr("AdvSys interpreter")})
            break
        case "adrift":
            mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "scareremglk", "game": filePath, "pageTitle": i18n.tr("Adrift interpreter")})
            break
        case "agt":
            mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "agilityremglk", "game": filePath, "pageTitle": i18n.tr("Agility interpreter")})
            break
        case "blorbed glulx":
        case "glulx":
            mainPageStack.push(Qt.resolvedUrl("../playGlulx.qml"),{"game": filePath})
            break
        case "blorbed zcode":
        case "zcode":
            mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "bocfelremglk", "game": filePath, "pageTitle": i18n.tr("Bocfel interpreter")})
            break
        case "hugo":
            mainPageStack.push(Qt.resolvedUrl("../PlayHugo.qml"),{"game": filePath})
            break
        case "level9":
            mainPageStack.push(Qt.resolvedUrl("../playLevel9.qml"),{"game": filePath})
            break
        case "magscrolls":
            mainPageStack.push(Qt.resolvedUrl("../playMagnetic.qml"),{"game": filePath})
            break
        case "tads3":
        case "tads2":
            mainPageStack.push(Qt.resolvedUrl("../playTads2.qml"),{"game": filePath})
            break
        case "executable":
            //Check is it's a .dat or a better solution
            switch (filePath.slice(-4)) {
                case ".zip":
                    console.log("Zip file")
                    break
                case ".agt":
                case ".D$$":
                    mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "agilityremglk", "game": filePath, "pageTitle": i18n.tr("Agility interpreter")})
                    break
                //Default to scott is not a good idea
                default:
                    mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "scottremglk", "game": filePath, "pageTitle": i18n.tr("Scott Adams interpreter")})
                    break
            }
            break
        default:
            console.log("We didnt find the format. Format: " + gameFormat)
    }
}

function readBabelResult(babelResult) {
    var babelResultArray = babelResult.trim().split("\n")
    for (var j=0; j<babelResultArray.length; j++) {
        if (babelResultArray[j].indexOf("Format:") !== -1) {
            gameFormat = babelResultArray[j].trim();
            gameFormat = gameFormat.slice(gameFormat.indexOf("Format: ") + 8);
        }

        if (babelResultArray[j].indexOf("IFID") !== -1) {
            gameIFID = babelResultArray[j].trim().split(" ")[1]
        }

        console.log("babel " + j + " " + babelResultArray[j])

    }
}
