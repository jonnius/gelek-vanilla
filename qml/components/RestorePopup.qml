import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import GelekBackend 1.0

Dialog {
    id: dialogueRestore

    property bool gameRestoredOrCancelled: false

    title: i18n.tr("Restore Game")
    text: savedGamesModel.count === 0 ? i18n.tr("There's no saved games yet") : i18n.tr("Choose a saved game to restore")

    ScrollView {
        height: savedGamesModel.count === 0 ? units.gu(1) : units.gu(25)

        ListView {
            id: savedGameList
            //interactive: false
            width: parent.width
            clip: true

            model: savedGamesModel
            delegate: savedGamesDelegate
        }
    }

    Component {
        id: savedGamesDelegate
        //TO DO: Use ListItemLayout instead?
        Row {
            height: units.gu(7)
            spacing: units.gu(3)
            width: parent.width

            Icon { width: units.gu(3); height: width; source: "../../assets/save.svg"; anchors.verticalCenter: parent.verticalCenter
                MouseArea {
                    anchors.fill: parent

                    onClicked: restoreGame()
                }
            }

            Text { text: fileName ; width: parent.width * .75; anchors.verticalCenter: parent.verticalCenter; clip: true
                MouseArea {
                    anchors.fill: parent

                    onClicked: restoreGame()
                }
            }

            function restoreGame() {
                GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "/home/phablet/.cache/gelek-vanilla.cibersheep/SavedGames/' + fileName + '" }')
                gameRestoredOrCancelled = true
                PopupUtils.close(dialogueRestore)
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "" }')
            gameRestoredOrCancelled = true
            PopupUtils.close(dialogueRestore)
        }
    }

    SavedGamesModel {
        id: savedGamesModel
    }

    Component.onDestruction: {
        //Prevent remGlk terp stop when Dialog is dismissed by Esc key stroke
        if (!gameRestoredOrCancelled) {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt"}')
        }
    }
}
