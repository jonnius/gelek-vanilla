import QtQuick 2.9
import Ubuntu.Components 1.3

PageHeader {
    title: root.title
    StyleHints {
        foregroundColor: lighterColor
        backgroundColor: darkColor
        dividerColor: lightColor
    }
}
