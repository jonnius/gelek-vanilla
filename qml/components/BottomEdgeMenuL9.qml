import QtQuick 2.4
import Ubuntu.Components 1.3

import "CommonActions"

BottomEdge {
    id: bottomEdge
    height: units.gu(38)

    hint {
        text: i18n.tr("Actions")
        enabled: !isLandscape || !settings.showMenuToTheRight
        visible: !isLandscape || !settings.showMenuToTheRight
    }

    preloadContent: true

    onCollapseStarted: bottomEdgeVisible = false

    onCollapseCompleted: {
        command.focus = true
        flickText.start()
    }

    onCommitStarted: {
        bottomEdgeVisible = true
        command.focus = false
    }

    contentComponent: Page {
        id: commonActions
        width: bottomEdge.width
        height: bottomEdge.height

        header: PageHeader {
            id: levelBottomEdgeHeader
            title: i18n.tr("Common Actions")
            StyleHints {
                foregroundColor: darkColor
                backgroundColor: lighterColor
                dividerColor: lighterColor
            }
        }

        CommonActionsL9 {
            anchors.top: levelBottomEdgeHeader.bottom
        }
    }
}
