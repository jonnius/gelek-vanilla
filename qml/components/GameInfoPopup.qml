import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import Ubuntu.DownloadManager 1.2
import QtQuick.XmlListModel 2.0

import "../js/gameFormatHelper.js" as Format
import GelekBackend 1.0

Dialog {
    id: infoDialog
    title: i18n.tr("Game Info")

    property string gameURLtoGetInfoFrom
    property string fileType

    Component.onCompleted: {
        GelekBackend.babel("-ifid", gameURLtoGetInfoFrom)
        GelekBackend.babel("-format", gameURLtoGetInfoFrom)

        //Leave this for later. We don't use it yet
        //GelekBackend.babel("-identify", gameURLtoGetInfoFrom)

        ifictionFile = ""

        var gameRoute = gameURLtoGetInfoFrom.split(/\//g)
        var folderURL = gameURLtoGetInfoFrom.slice(0, gameURLtoGetInfoFrom.indexOf(gameRoute[gameRoute.length -1]))
        ifictionFile = folderURL + gameIFID + ".ifiction"
        gameCover = folderURL + gameIFID
        console.log("Debug: " + ifictionFile + "\n" + gameCover)

        image.source = gameCover
        xmlModel.reload()
    }

    UbuntuShape {
        id: iconTop
        //width: units.gu(5)
        height: iconTop.width
        anchors.horizontalCenter: parent.horizontalCenter
        aspect: UbuntuShape.Flat
        sourceFillMode: UbuntuShape.PreserveAspectFit

        source: Image {
            id: image

            //Set the source image for cover onCompleted
            //https://gitlab.com/cibersheep/gelek-vanilla/issues/1

            onStatusChanged: {
                if (image.status == Image.Error) {
                    image.source = "../../assets/gelek.svg"
                }
            }
        }
    }

    ListView {
        id: ifictionListView
        width: parent.width
        height: xmlModel.count * units.gu(10)
        interactive: false
        model: xmlModel
        delegate: xmlDelegate
    }

    Text {
        id: ifidText
        text: "IFID: " + (gameIFID || "")
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }

    Text {
        id: formatText
        text: "Format: " + (gameFormat || "")
    }

    Label {
        id: alertText
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignCenter
        visible: xmlModel.count == 0
        text: i18n.tr("Ifiction file doesn't exist!")
    }

    Rectangle {
        id: downloadingBar
        visible: downloadCover.downloadInProgress || downloadIfile.downloadInProgress
        height: downloadFileButton.height

        ProgressBar {
            anchors.verticalCenter: parent.verticalCenter
            visible: parent.visible
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - units.gu(2)
            value: (width / 300) * (downloadCover.progress + downloadIfile.progress)
        }
    }

    Button {
        id: downloadFileButton
        text: xmlModel.count == 0
            ? i18n.tr("Download")
            : i18n.tr("Re-download")

        visible: !downloadingBar.visible

        onClicked: {
            //If reload, delete files first or the new are not downloaded
            if (ifictionFile !== "") {
                GelekBackend.deleteSavedGames(ifictionFile)
                GelekBackend.deleteSavedGames(gameCover)
            }

            downloadIfile.download(Qt.resolvedUrl("http://ifdb.tads.org/viewgame?ifiction&ifid=" + gameIFID))
            downloadCover.download(Qt.resolvedUrl("http://ifdb.tads.org/viewgame?coverart&ifid=" + gameIFID))
        }
    }

    Button {
        text: downloadCover.downloadInProgress || downloadIfile.downloadInProgress
            ? i18n.tr("Cancel")
            : i18n.tr("Close")

        onClicked: {
            if (downloadCover.downloadInProgress || downloadIfile.downloadInProgress) {
                downloadIfile.cancel();
                downloadCover.cancel();
            } else
                mainPageStack.pop();
        }
    }

    SingleDownload {
        id: downloadIfile
        autoStart: true

        onFinished: {
            console.log("Download path: " + path)
            //Saved ifictionFile route is returned by storeGameFile()
            ifictionFile = GelekBackend.storeGameFile(path, gameIFID + ".ifiction")
            xmlModel.reload()
        }

        onErrorMessageChanged: {
            console.log("Error downloading file:",errorMessage)
        }
    }

    SingleDownload {
        id: downloadCover
        autoStart: true

        onFinished: {
            console.log("Download cover path: " + path)
            //Let's store the downloaded iFiction file in the same folder as the game
            //Saved cover route is returned by storeGameFile()
            gameCover = GelekBackend.storeGameFile(path, gameIFID)
            image.source = gameCover
        }
    }

    XmlListModel {
        id: xmlModel
        source: ifictionFile
        query: "/ifindex/story"
        namespaceDeclarations: "declare default element namespace 'http://babel.ifarchive.org/protocol/iFiction/';"
        XmlRole { name: "title"; query: "bibliographic/title/string()" }
        XmlRole { name: "author"; query: "bibliographic/author/string()" }
        XmlRole { name: "language"; query: "bibliographic/language/string()" }
        XmlRole { name: "firstpublished"; query: "bibliographic/firstpublished/string()" }
        XmlRole { name: "cover"; query: "ifdb/coverart/url/string()" }
    }

    Component {
        id: xmlDelegate

        Column {
            width: parent.width
            spacing: units.gu(2)

            Text {
                id: titleText
                text: title + ", " + firstpublished + " (" + language + ")"
                font.pointSize: units.gu(1.4)
                font.bold: true
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            Text {
                id: authorsText
                text: "Authors: " + author
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }
        }
    }
}
