import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import GelekBackend 1.0

Dialog {
    id: dialogue

    property bool nameExists: false
    property bool gameSavedOrCancelled: false

    title: i18n.tr("Save Game")
    text: i18n.tr("Enter a name to save the game")

    Label {
        id: alertText
        horizontalAlignment: Text.AlignCenter
        visible: nameExists
        text: i18n.tr("File name already exists!")
        color: theme.palette.normal.negative
    }

    TextField {
        id: saveGameName
        onAccepted: saveButton.clicked()
    }

    Button {
        id: saveButton
        text: i18n.tr("Save")
        color: saveGameName.text ==="" || saveGameName.text === " "
            ? mainView.darkTheme
                ? "#888"
                : "#666"
            : theme.palette.normal.positive
        enabled: saveGameName.text !=="" && saveGameName.text !== " "

        onClicked: {
            nameExists = false;
            for (var i=0; i < savedGamesModel.count; i++){
                if (savedGamesModel.get(i, "fileBaseName") === saveGameName.text) {
                    console.log("Debug: File name exists")
                    nameExists = true
                    alertFade.start()
                    break
                }
            }

            if (!nameExists) {
                console.log("Debug: Game name to save: " + saveGameName.text)
                console.log("QDATE " + Qt.formatDateTime(new Date(), "dd-MM-yyyy, HH:mm:ss"))

                GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "' + mainView.savedGamesURL + saveGameName.text + '.glksave" }')
                GelekBackend.addToList(saveGameName.text + '.glksave', gameName, Qt.formatDateTime(new Date(), "dd-MM-yyyy, HH:mm:ss"))
                gameSavedOrCancelled = true
                PopupUtils.close(dialogue)
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "" }')
            gameSavedOrCancelled = true
            PopupUtils.close(dialogue)
        }
    }

    SavedGamesModel {
        id: savedGamesModel
    }

    Timer {
        id: alertFade
        interval: 3000

        onTriggered: nameExists = false
    }

    Component.onDestruction: {
        //Prevent remGlk terp stop when Dialog is dismissed by Esc key stroke
        if (!gameSavedOrCancelled) {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt"}')
        }
    }
}
