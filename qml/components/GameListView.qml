import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../js/gameFormatHelper.js" as Format
import GelekBackend 1.0

Component {
    ListItem {
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        divider.visible: false
        clip: true
        highlightColor: selectionColor

        leadingActions: ListItemActions {
            actions: Action {
                iconName: "delete"
                text: i18n.tr("Delete")

                onTriggered: {
                    GelekBackend.babel("-ifid", filePath)
                    var gameRoute = filePath.split(/\//g)
                    var folderURL = filePath.slice(0, filePath.indexOf(gameRoute[gameRoute.length -1]))
                    ifictionFile = folderURL + gameIFID + ".ifiction"
                    gameCover = folderURL + gameIFID
                    GelekBackend.deleteSavedGames(ifictionFile)
                    GelekBackend.deleteSavedGames(gameCover)
                    GelekBackend.deleteSavedGames(filePath)
                }
            }
        }

        trailingActions: ListItemActions {
            actions: Action {
                iconName: "info"
                text: i18n.tr("Info")

                onTriggered: {
                    mainPageStack.push(Qt.resolvedUrl("GameInfoPopup.qml"),{"gameURLtoGetInfoFrom": filePath});
                }
            }
        }

        ListItemLayout {
            id:layout
            title.text: fileName
            width: parent.width - marginColumn * 9
            anchors.horizontalCenter: parent.horizontalCenter

            Icon {
                source: "../../assets/game.svg"
                SlotsLayout.position: SlotsLayout.Leading
                width: units.gu(3)
            }
        }

        onClicked: {
            GelekBackend.babel("-format", filePath)
            GelekBackend.babel("-ifid", filePath)
            Format.startGameByFormat(filePath, gameFormat)
        }
    }
}
