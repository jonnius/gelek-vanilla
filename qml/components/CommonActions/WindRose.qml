import QtQuick 2.4
import Ubuntu.Components 1.3

Grid {
    height: Math.min(parent.width - units.gu(4), ((parent.height  - units.gu(2))/ 5) * 3)
    width: height
    id: navigation
    anchors.horizontalCenter: parent.horizontalCenter
    property real windRoseWidth: width / 3
    columns: 3

    Icon { id: nw; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/nw.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "NW"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: up; color: lightColor; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/n.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "N"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: ne; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/ne.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "NE"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: w; color: lightColor; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/w.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "W"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: other; color: lightColor; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/centre.svg"}

    Icon { id: e; color: lightColor; width:windRoseWidth; height: width; anchors.leftMargin: units.gu(2); source: "../../../assets/bottomedge/e.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "E"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: sw; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/sw.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "SW"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: s; color: lightColor; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/s.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "S"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }

    Icon { id: se; width: windRoseWidth; height: width; source: "../../../assets/bottomedge/se.svg"
        MouseArea {
                anchors.fill: parent;
                onClicked: {
                    command.text = "SE"
                    instruction.clicked()
                    bottomEdge.collapse()
                    command.focus = true
               }
        }
    }
}
