import QtQuick 2.4
import Ubuntu.Components 1.3

Button {
    property string iconShown
    property string txtShown
    property string txtCommand
    property string iconsourceShown

    width: units.gu(4)
    color: lightColor

    Icon {
        id: icon
        width: units.gu(2)
        anchors.centerIn: parent
        color: "#fff" //lighterColor
        name: iconShown
    }
    
    Component.onCompleted: if (iconsourceShown !== "") icon.source = iconsourceShown

    onClicked: {
        command.text = txtCommand
        if (bottomEdgeVisible) bottomEdge.collapse()
        command.focus = true
        flickText.start()
    }
}
