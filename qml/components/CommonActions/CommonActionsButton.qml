import QtQuick 2.4
import Ubuntu.Components 1.3

Button {
    property string txtShown
    property string txtCommand

    width: parent.width
    color: lightColor
    text: txtShown

    onClicked: {
        command.text = txtCommand
        if (bottomEdgeVisible) bottomEdge.collapse()
        command.focus = true
        flickText.start()
    }
}
