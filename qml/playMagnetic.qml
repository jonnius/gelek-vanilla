import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Qt.labs.folderlistmodel 2.1

import "components"
import "components/CommonActions"
import "js/jsonParser.js" as JsParser

import GelekBackend 1.0

//TODO:
//Refresh window width on glk intead of resizing font pixel
//Start Wonder and type E

Page {
    id: playLevel9

    property string game
    property string gameName:""
    property string completeOut
    property var    response
    property bool   bottomEdgeVisible: false

    property int    canvasWidth: 324
    property int    canvasHeight: 0 //225

    property bool   renderingImage: false
    property var    commandsBuffer: []
    property int    oldGeneration: 0
    readonly property int untilTheEnd: -1

    property int viewboxw: -1
    property int viewboxh: -1
    property int clipWidth: -1
    property int clipHeight: -1

    property int windRoseWidth: units.gu(4) //(parent.height - units.gu(4)) / 9
    property int screenWidth: 80
    property int gridHeight: 0

    anchors.fill: parent

    header: GelekHeader {
        id: gameHeader
        title: i18n.tr("Magnetic Scrolls interpreter")
    }

    Component.onCompleted: {
        GelekBackend.launchTerp("magneticremglk", game)
        GelekBackend.sendCommand('{ "type": "init", "gen": 0, "metrics": { "width":1000, "height":800, "charwidth":12, "charheight":14 },  "support": [ "graphics", "graphicswin", "timer" ] }')
    }

    Connections {
        target: GelekBackend

        //let's get the answer from GLK
        onStdoutLevel:{
            response = ""
            var stdoutResponse = GelekBackend.readSOL()

            while (stdoutResponse.indexOf("{\"type\":\"error\",") !== -1) {
                var endOfError = stdoutResponse.indexOf("}");
                console.log("GLK error: " + stdoutResponse.slice(0,endOfError+1).replace(/\n/g, ""));
                stdoutResponse = stdoutResponse.replace(/{\"type\":\"error\",.*}/i, "");
            }

            completeOut += stdoutResponse

            //Check if we got a complete json from stdout
            //CAREFUL. This comment is to make "{" marks work properly on editor
            if (JsParser.isJsonFinished(completeOut)) {
                if (settings.debugging) console.log("We have a json");
                if (settings.debugging) console.log("| json stdout -------------------\n" + completeOut + "\n----------------------");
                response = JSON.parse(completeOut);
                completeOut = "";
                JsParser.formatText(response);
            } else
                if (settings.debugging) console.log("Not a json. Waiting for next info");
        }

        //let's get the answer from Babel
        onStdoutBabel: {
            var babelResult = GelekBackend.readBabelOut()
        }
    }

    UbuntuShape {
        id: gameImage
        visible: settings.showPicturesInGame && viewboxw !== -1
        width: isLandscape
            ? settings.pictureToTheRight
                ? Math.min(Math.max(clipWidth, (parent.width  - units.gu(6)) * .45), (parent.height -units.gu(10)) / 3  * (clipWidth/clipHeight) + 6)
                : Math.min(Math.max(clipWidth, (parent.width  - units.gu(6)) * .55), (parent.height -units.gu(10)) / 3  * (clipWidth/clipHeight) + 6)
            : Math.min(Math.max(clipWidth, parent.width - units.gu(2)), (parent.height -units.gu(10))/ 3 * (clipWidth/clipHeight) + 6)
        height: width * (clipHeight/clipWidth) + 6
        backgroundColor: Theme.palette.normal.background

        /*
         * This would make the image to be wider as the text
           width: txt.width
           height: width* (img.height / img.width) -6
         */

        aspect: UbuntuShape.Flat

        anchors {
            horizontalCenter: settings.pictureToTheRight && isLandscape
                ? commonsActionsColumnL9.horizontalCenter
                : gameFlickable.horizontalCenter
            top: gameHeader.visible
                ? gameHeader.bottom
                : parent.top
        }

        sourceFillMode: UbuntuShape.PreserveAspectCrop
        sourceVerticalAlignment: UbuntuShape.AlignVCenter
        sourceHorizontalAlignment: UbuntuShape.AlignHCenter
        sourceScale: Qt.vector2d(Math.min(canvasWidth / clipWidth, canvasHeight / clipHeight), Math.min(canvasWidth / clipWidth, canvasHeight / clipHeight) )

        source: Canvas {
            id: img
            width: canvasWidth
            height: canvasHeight
            //Deprecated canvasWindow
            //canvasWindow: Qt.rect(viewboxw, viewboxh, clipWidth, clipHeight)
        }
    }

    Flickable {
        id: gameFlickable
        clip: true
        contentHeight: txt.height + units.gu(5)
        width: isLandscape && (commonsActionsColumnL9.visible || (settings.pictureToTheRight && gameImage.visible))
            ? parent.width * 0.55
            : parent.width

        anchors {
            //Flickable should anchor at the bottom of the image except that is at the right
            top: gameImage.visible && (!isLandscape || !settings.pictureToTheRight)
                ? gameImage.bottom
                : gameHeader.visible
                    ? gameHeader.bottom
                    : parent.top

            bottom: inputArea.top
            bottomMargin: units.gu(1)
        }

        Column{
            id: levelMainText
            anchors.fill:parent
            spacing: units.gu(2)
            anchors.margins:units.gu(2)

            Text {
                id:txt
                width: parent.width
                textFormat: Text.RichText
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                horizontalAlignment: Text.AlignJustify
                color: Theme.palette.normal.backgroundText
            }
        }
    }

    Rectangle {
        id: gridLinesRec
        anchors.top: parent.top
        anchors.topMargin: gameHeader.visible ? gameHeader.height : 0
        height: gridLines.height
        width: gameFlickable.width

        ListView {
            id: gridLines
            interactive: false
            width: parent.width
            anchors.centerIn: parent.centerIn

            height: units.gu(3) * gridHeight

            model: gridModel
            delegate: gridDelegate
        }
    }

    HideHeader {}

    Row {
        id: inputArea
        spacing: units.gu(1)
        width: gameFlickable.width - instruction.width

        anchors {
            left: parent.left
            right: isLandscape && commonsActionsColumnL9.visible ? undefined : parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }

        TextField{
            id: command
            width: parent.width - instruction.width - units.gu(1)
            onTextChanged: if (text.length === 1 && response.input[0].type === "char") instruction.clicked()

            onAccepted: {
                command.focus=false;
                command.focus=true;
                instruction.clicked()
            }

            onFocusChanged: {
                if (!command.focus && !bottomEdgeVisible) {
                    command.focus = true;
                }
            }
        }

        Button {
            //TODO: Up and Down arrows: \\224;72 (up)
            id:instruction
            width: units.gu(4)

            Icon {
                width: units.gu(2)
                anchors.centerIn: parent
                color: lighterColor
                name: "keyboard-enter"
            }

            color: command.text === "" ? UbuntuColors.graphite : darkColor

            onClicked:{
                JsParser.commandToBuffer(command.text);
                command.text = "";
            }
        }
    }

    BottomEdgeMenuL9 {
        id: bottomEdge
    }

    Component {
        id: showPopUpSave
        SavePopup { }
    }

    Component {
        id: showPopUpRestore
        RestorePopup { }
    }

    //This element is the grid = every line is a Row of Text elements (to be able to mix formats and colors)
    Component {
        id: gridDelegate

        Row {
            id: lineDelegate
            width: parent.width
            height: units.gu(1.7 * 12 * gameFlickable.width / 1000 / units.gu(1))
            clip: true

            Repeater {
                model: singleRow

                Text {
                    id: gridTxt
                    text: model.text
                    color: model.color
                    font.bold: model.bold
                    font.italic: model.italic
                    verticalAlignment: Text.AlignVCenter

                    height: parent.height
                    font.family: "monospace"
                    fontSizeMode: Text.FixedSize
                    font.pixelSize: units.gu(1.7 * 12 * gameFlickable.width / 1000 / units.gu(1))

                    Rectangle {
                        z: -1
                        anchors.fill: gridTxt
                        width: gridTxt.width
                        color: model.bgcolor
                    }
                }
            }
        }
    }

    ListModel {
        id: gridModel

        property var singleRow: []

        function initialize() {
            gridModel.clear();
        }
    }

    Connections {
        target: Qt.inputMethod

        onVisibleChanged: flickText.start()
    }

    Timer {
        id: flickText
        interval: 10
        onTriggered: {
            for(var i=0; i < Qt.inputMethod.keyboardRectangle.height && !gameFlickable.atYEnd; i++) {
                gameFlickable.contentY += units.gu(1);
                i++;
            }
        }
    }


    Timer {
        id: retrieveTimer
        repeat: true

        onTriggered: {
            if (response.gen) {
                //Process on command in the commandBuffer
                JsParser.processBuffer()
            } else {
                if (settings.debugging) console.log("retrieveTimer triggered but we have no response.gen. We are doing nothing");
            }
        }
    }

    CommonActionsColumn {
        id: commonsActionsColumnL9
        visible: isLandscape && settings.showMenuToTheRight

        anchors {
            top: gameImage.visible && settings.pictureToTheRight
                ? gameImage.bottom
                : gameHeader.visible
                    ? gameHeader.bottom
                    : parent.top
            left: gameFlickable.right
            right: parent.right
        }
    }

    Component.onDestruction: JsParser.terminateTerp();
}
