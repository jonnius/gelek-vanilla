import QtQuick 2.4
import Ubuntu.Components 1.3
//import Ubuntu.Components.ListItems 1.3 as ListItemm
import "components"
import Qt.labs.folderlistmodel 2.1
import GelekBackend 1.0
import Ubuntu.Content 1.3

Page {
    id: savedGamesLevel9
    anchors.fill: parent

    header: GelekHeader {
        id: levelSavedGamesHeader
        title: i18n.tr("Saved Games")
        flickable: savedGameSnowball
    }

    property int sectionNumber

    ListView {
        id: savedGameSnowball
        width: parent.width - marginColumn * 2
        height: units.gu(7) * categorizedSavedGamesModel.count + units.gu(5) + units.gu(7) * (sectionNumber +1)

        anchors {
            fill: parent
            //topMargin: levelSavedGamesHeader.height
            horizontalCenter: parent.horizontalCenter
        }

        header: Column {
            id: savedGameTitle
            spacing: units.gu(2)
            width: parent.width

            Label {
                text: "List of Saved Games"
                color: darkColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
                width: parent.width
                height: units.gu(10)

                font {
                    bold: true
                    pointSize: units.gu(2)
                }
            }

            Text {
                color: theme.palette.normal.backgroundSecondaryText
                text: savedGamesModel.count === 0
                    ? "No saved games found"
                    : "Slide for options"
                anchors.horizontalCenter: parent.horizontalCenter
            }
        }

        section {
            property: "category"
            criteria: ViewSection.FullString

            delegate: ListItemHeader {
                title: section
            }
        }

        model: sortedSavedGamesModel
        delegate: savedGameSnowballDelegate
    }

    Component {
        id: savedGameSnowballDelegate

        ListItem {
            property string wichGame;

            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            leadingActions: ListItemActions {
                actions: Action {
                    iconName: "delete"
                    text: i18n.tr("Delete")

                    onTriggered: {
                        GelekBackend.deleteSavedGames(filePath)
                        console.log(fileName)
                        GelekBackend.deleteSave(fileName)
                    }
                }
            }

            trailingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "share"
                        text: i18n.tr("Share")

                        onTriggered: {
                            //Open the click with Telegram, OpenStore, etc.
                            var sharePage = mainPageStack.push(Qt.resolvedUrl("SharePage.qml"), {"url": filePath, "contentType": ContentType.All, "handler": ContentHandler.Share});
                            sharePage.imported.connect(function(filePath) {
                                mainPageStack.push(Qt.resolvedUrl("savedGamesPage.qml"))
                            })
                        }

                    },
                    Action {
                        iconName: "save"
                        text: i18n.tr("Save")

                        onTriggered: {
                            //Save with File Manger, etc.
                            var InstallPage = mainPageStack.push(Qt.resolvedUrl("InstallPage.qml"), {"url": filePath, "contentType": ContentType.All, "handler": ContentHandler.Destination});
                            InstallPage.imported.connect(function(filePath) {
                                mainPageStack.push(Qt.resolvedUrl("savedGamesPage.qml"))
                            })
                        }
                    }
                ]
            }

            ListItemLayout {
                id: showItem
                title.text: fileBaseName

                Icon {
                    width: units.gu(3); height: width
                    source: "../assets/save.svg"
                    SlotsLayout.position: SlotsLayout.Leading
                }
            }
        }
    }

    FolderListModel {
        id: savedGamesModel
        rootFolder: savedGamesURL
        folder: savedGamesURL
        //With Terps that let you insert filename, savegames can be of any (or no) file extension
        //nameFilters: [ "*.glksave", "*.GLKSAVE" ]
        showHidden: true
        showDirs: false
        sortField: FolderListModel.Time
        onDataChanged: { updateModels(); console.log("Data changed") }
        onModelReset: console.log("Model Reset")
        onColumnsAboutToBeInserted: console.log("Column about to be inserted")
        onColumnsInserted: console.log("Column inserted")

        onCountChanged: updateModels()

        function updateModels() {
            console.log("Updating Models")
            categorizedSavedGamesModel.clear()
            var gameName = "";

            for (var i=0; i< savedGamesModel.count; ++i) {
                if (savedGamesModel.get(i, "fileName").indexOf("gelekSaveList") == -1 && savedGamesModel.get(i, "fileName").indexOf("gelekSaveListOLD") == -1) {
                    gameName = GelekBackend.whichGame(savedGamesModel.get(i, "fileName"));
                    if (gameName == "")
                        gameName = i18n.tr("Unknown");

                    categorizedSavedGamesModel.append({
                        category: gameName,
                        fileName: savedGamesModel.get(i, "fileName"),
                        fileBaseName: savedGamesModel.get(i, "fileBaseName"),
                        filePath: savedGamesModel.get(i, "filePath")
                    });
                }
            }
        }
    }

    ListModel {
        id: categorizedSavedGamesModel
    }

    SortFilterModel {
        id: sortedSavedGamesModel
        model: categorizedSavedGamesModel

        sort {
            property: "category"
            order: Qt.DescendingOrder
        }
    }
}
