import QtQuick 2.4
import Ubuntu.Components 1.3

import "components"
import Ubuntu.Content 1.3
import Qt.labs.settings 1.0
import Ubuntu.DownloadManager 1.2
import QtQuick.XmlListModel 2.0

import Qt.labs.folderlistmodel 2.1
import "js/gameFormatHelper.js" as Format
import GelekBackend 1.0

MainView {
    id: mainView

    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    applicationName: "gelek-vanilla.cibersheep"
    width: units.gu(40)
    height: units.gu(75)

    //Margins
    property int marginColumn: units.gu(1)

    //Colors
    property string   lighterColor: UbuntuColors.porcelain
    property bool        darkTheme: theme.name === "Ubuntu.Components.Themes.SuruDark"
    property string selectionColor: darkTheme ? "#33949494" : "#335e5c59"
    property string     lightColor: darkTheme ? "#8c7f84" : "#75646a"
    property string      darkColor: darkTheme ? "#6a5a60" : "#4d4246"

    property string gameToAdd: ""
    property string gameFormat: ""
    property string gameIFID: ""
    property string gameCover: ""
    property string ifictionFile: ""
    //TODO: Use QtStandardPath
    property string baseRootURL: "/home/phablet/.cache/" + applicationName + "/"
    property string gamesURL: baseRootURL + "Games/"
    property string savedGamesURL: baseRootURL + "SavedGames/"
    property string gamesFilesURL: baseRootURL + "GameFiles/"

    //Settings properties
    property var settings: Settings {
        property bool convergenceMode: true
        property bool pictureToTheRight: false
        property bool showMenuToTheRight: true
        property bool showPicturesInGame: true
        property bool dontForceFocus: false
        property bool haveCreatedFolders: false
        property bool debugging: false
    }

    property bool isLandscape: settings.convergenceMode && width > height + units.gu(10) // Consider the keyboard height

    anchorToKeyboard: true

    Connections {
        target: GelekBackend

        //let's get the answer from Babel
        onStdoutBabel: {
            //ToFix: We sould reset gameFormat
            var babelResult = GelekBackend.readBabelOut();
            Format.readBabelResult(babelResult);
            if (settings.debugging) console.log(
                "--------------------------\n",
                "Babel says: ", babelResult,
              "\nFormat is:  ", gameFormat,
              "\n--------------------------\n"
            )
        }
    }

    PageStack {
        id: mainPageStack
        anchors.fill: parent

        Component.onCompleted: {
            if (!settings.haveCreatedFolders) {
                GelekBackend.firtRunFolderSetUp();
                settings.haveCreatedFolders = true;
            }

            mainPageStack.push(pageMain);

            //Fixes: No games on the list after first import
            importedGamesModel.rootFolder = gamesURL
            importedGamesModel.folder = gamesURL
        }

        Page {
            id: pageMain
            anchors.fill: parent
            header: GelekHeader {
                id: pageHeader
                title: i18n.tr("Gelek Vanilla")
                flickable: flickable
            }

            Flickable {
                id: flickable
                clip: true

                anchors {
                    top: pageHeader.bottom
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }

                contentHeight: contentItem.childrenRect.height + units.gu(2)

                Column {
                    id: addGame
                    width: parent.width

                    anchors {
                        top: parent.top
                        topMargin: units.gu(2)
                    }

                    spacing: units.gu(1)

                    Icon {
                        id: importGame
                        width: units.gu(6)
                        height: width
                        anchors.horizontalCenter: parent.horizontalCenter
                        name: "add"
                    }

                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: i18n.tr("Import")
                        color: theme.palette.normal.backgroundText
                        textSize: Label.XLarge
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    }

                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: theme.palette.normal.backgroundText
                        text: i18n.tr("a new game")
                    }

                    Text {
                        text: i18n.tr("Accepted formats: %1").arg(
                            "adrift (3, 4), agt, advsys, glulx, hugo, " +
                            "level9, magnetic scrolls, scott adams, tads, zcode"
                        )

                        width: parent.width - marginColumn * 12
                        anchors.horizontalCenter: parent.horizontalCenter
                        horizontalAlignment: Text.AlignHCenter
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        color: theme.palette.normal.backgroundTertiaryText
                    }
                }

                MouseArea {
                    anchors.fill: addGame

                    onClicked: {
                        var importPage = mainPageStack.push(
                            Qt.resolvedUrl("ImportPage.qml"),{
                            "contentType": ContentType.All,
                            "handler": ContentHandler.Source
                        })

                        importPage.imported.connect(function(fileUrl) {
                            gameToAdd  = fileUrl
                        })
                    }
                }

                Column {
                    id: previousGames
                    width: parent.width

                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        top: addGame.bottom
                        topMargin: units.gu(10)
                    }

                    Label {
                        height: units.gu(4)
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: importedGamesModel.count === 0 ? i18n.tr("No Imported Games Yet") : i18n.tr("Previouly Imported Games")
                        color: lightColor
                    }

                    FolderListModel {
                        id: importedGamesModel
                        rootFolder: gamesURL
                        folder: gamesURL
                        nameFilters: [
                            "*.Z*", "*.z*", "*.sna", "*.dat", "*.tap", "*.l9", "*.st", "*.atr", "*.com", "*.bin",
                            "*.tzx", "*.mag", "*.ulx", "*.gblorb", "*.gam", "*.blb", "*.t3", "*.agx", "*.taf", "*.hex",
                            "*.SNA", "*.DAT", "*.L9", "*.ST", "*.ATR", "*.BIN", "*.COM", "*.TAP", "*.TZX", "*.MAG",
                            "*.ULX", "*.GBLORB", "*.BLB", "*.T3", "*.AGX", "*.D$$", "*.TAF", "*.HEX"
                        ]
                        showHidden: true
                        showDirs: false
                        sortField: FolderListModel.Time
                    }

                    GameListView {
                        id: importedGamesDelegate
                    }

                    ListView {
                        id: importGameList
                        interactive: false
                        width: parent.width

                        anchors.horizontalCenter: parent.horizontalCenter
                        height: units.gu(7) * importedGamesModel.count

                        model: importedGamesModel
                        delegate: importedGamesDelegate
                    }
                }

                Column {
                    id: otherOptions
                    width: parent.width

                    anchors {
                        top: previousGames.bottom
                        leftMargin: marginColumn
                        rightMargin: marginColumn
                        topMargin: units.gu(6)
                    }

                    Divider { }

                    ListItem {
                        width: parent.width
                        divider.visible: true

                        GelekItemLayout {
                            title.text: i18n.tr("Saved Games")
                        }

                        onClicked: {
                            Qt.inputMethod.hide();
                            mainPageStack.push(Qt.resolvedUrl("savedGamesPage.qml"));
                        }
                    }
                }
            }
        }
    }
}


