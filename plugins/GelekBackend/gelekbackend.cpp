#include "gelekbackend.h"
#include <QDebug>
#include <QDir>
#include <QObject>
#include <QProcess>
//Jujuyeh
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QString>

#include <iostream>
#include <cstring>

#include <QStandardPaths>

GelekBackend::GelekBackend() {
}

const bool debuggingMode = false;

//Don't define here url. That doesn't work on desktop

//Let's get the current path for terps
QDir terpsFolder;
const QString utilsPath = terpsFolder.absoluteFilePath("terps/");

//TODO: Define constant to be used in qml
/*
const int cacheLocation = 1;
const int appConfigLocation = 2;
const int appLocalDataLocation = 3;
*/

void GelekBackend::systemInfo() {
    qDebug() << utilsPath;
    qDebug() << "CacheLocation:        " << QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    qDebug() << "AppConfigLocation:    " << QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "ConfigLocation:       " << QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    qDebug() << "AppConfigLocation:    " << QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
    qDebug() << "TempLocation:         " << QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    qDebug() << "AppLocalDataLocation: " << QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    qDebug() << "ApplicationsLocation: " << QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation);
    qDebug() << "RuntimeLocation:      " << QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation);
}

bool GelekBackend::launchTerp(const QString &terp, const QString &game)
{
    QStringList args;
    QString interpreter;

    terpProcess = new QProcess(this);
    terpProcess->setReadChannel(QProcess::StandardOutput);
    connect(terpProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(processOutput()));

    //Workaround for old MagneticScroll games that saves straight to the working directory
    terpProcess->setWorkingDirectory(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    QTextStream(&interpreter) << utilsPath << terp;

    terpProcess->start(interpreter, QStringList() << game);

    if (!terpProcess->waitForStarted())
    {
        qDebug() << terp << " start failed";
        if (debuggingMode) {
            qDebug() << "Working directory: " << terpProcess->workingDirectory();
            qDebug() << "Interpreter: " << interpreter;
            qDebug() << "utilsPath: " << utilsPath;
        }
        return false;
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
    else 
    {
        qDebug() << terp << " started";
        if (debuggingMode) {
            qDebug() << "Working directory: " << terpProcess->workingDirectory();
            qDebug() << "Interpreter: " << interpreter;
            qDebug() << "utilsPath: " << utilsPath;
        }
        return true;
    }
}

void GelekBackend::stopTerp() {
    if (terpProcess->QProcess::Running) {
        qDebug() << "Terp was running. Terminating...";
        terpProcess->terminate();
        qDebug() << "Is finished?: " << terpProcess->waitForFinished();

        //We don't need the pointer anymore
        delete terpProcess;
        if (debuggingMode) {
            qDebug() << "And terpProcess deleted";
        }
    }
    else if (debuggingMode) {
        qDebug() << "Terp was not running";
    }
}

void GelekBackend::sendCommand(const QString &cmd) {
    if (terpProcess->state() != QProcess::Running) {
        return;
    }

    if (debuggingMode) {
        qDebug() << "[toGlk] " << cmd;
    }

    terpProcess->write(cmd.toLocal8Bit() + '\n');
}

//Send the answer from GLK to stdout
QString GelekBackend::readSOL() {
    QByteArray bytes = terpProcess->readAllStandardOutput();
    QString output = QString::fromLocal8Bit(bytes);
    
    if (debuggingMode) {
        qDebug() << "------ stdout output\n" << output;
        qDebug() << "\n------------------";
    }

    return output;
}

QString GelekBackend::storeGameFile(QString gameURL, QString gameName) {
    QDir storeDir;

    //Should be /home/phablet/.cache/gelek-vanilla.cibersheep/Games/
    storeDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/Games/";

    QFile copyGame;
    QDir storeGameDir;
    storeGameDir.mkpath(storeDir.absolutePath());
    //storeGameDir.setCurrent(storeDir);
    copyGame.copy(gameURL, storeDir.filePath(gameName));

    if (debuggingMode) {
        qDebug() << "gameName cpp: " << storeDir.filePath(gameName);
    }
    
    //Return path of the stored game
    return storeDir.filePath(gameName);
}

void GelekBackend::createSavedGamesDir() {
    QDir savedGamesDir;

    //Should be /home/phablet/.cache/gelek-vanilla.cibersheep/SavedGames/
    savedGamesDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/";
    savedGamesDir.mkpath(savedGamesDir.absolutePath());
}

void GelekBackend::deleteSavedGames(QString savedGame) {
    QFile removeSavedGame;

    qDebug() << "Deleting: " << savedGame;
    if (!removeSavedGame.remove(savedGame)) {
        qDebug() << "Error deleting file";
    }
}

QStringList GelekBackend::listFiles() {
	/*
	// Use, in qml, GelekBackend.listFiles() or FolderListModel
    QString gamesDir;
    gamesDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/HubIncoming/";

    QStringList filters;
    filters << "*.dat" << "*.l9" << "*.sna" << "*.DAT" << "*.L9" << "*.SNA" << "*.st" << "*.ST";

    QStringList gamesFiles;

    //QDirIterator it(dir, QStringList() << "*.dat" << "*.l9" << "*.sna", QDir::NoFilter, QDirIterator::Subdirectories);
    QDirIterator it(gamesDir, filters, QDir::NoFilter, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        gamesFiles.append(it.next());
    }

    //qDebug() << gamesFiles;
    return gamesFiles;
    */
}

void GelekBackend::babel(QString request, const QString &game)
{
    QStringList args;
    args << request << game;
    babelProcess = new QProcess(this);

    babelProcess->setReadChannel(QProcess::StandardOutput);
    connect(babelProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(babelOutput()));

    QString babelPath;
    QTextStream(&babelPath) << utilsPath << "babel-arm";

    babelProcess->start(babelPath, QStringList() << args);
    
    if (!babelProcess->waitForStarted()) {
        qDebug() << "Bable start failed";
    }
    else qDebug() << "Bable started";
    
    if (debuggingMode) {
        qDebug() << "Babel " << args;
    }

    //Wait for the process to finish and delete it    
    babelProcess->waitForFinished();
    delete babelProcess;
}

//Send the answer from Babel to stdout
QString GelekBackend::readBabelOut()
{
    QByteArray bytes = babelProcess->readAllStandardOutput();
    QString output = QString::fromUtf8(bytes);

    if (debuggingMode) {
        qDebug() << "--- Babel stdout output\n" << output;
        qDebug() << "\n-----------------------";
    }

    return output;
}

void GelekBackend::extractBlorb(const QString &game, const QString &ifid)
{
    QStringList args;
    QString blorbtool;

    blorbProcess = new QProcess(this);
    blorbProcess->setReadChannel(QProcess::StandardOutput);
    connect(blorbProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(blorbOutput()));

    //Create a game folder
    QDir gameFolder;
    gameFolder = QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/GameFiles/" + "/" + ifid;
    gameFolder.mkpath(gameFolder.absolutePath());

    blorbProcess->setWorkingDirectory(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/GameFiles/" + "/" + ifid);

    QTextStream(&blorbtool) << utilsPath << "blorbscan-arm";

    blorbProcess->start(blorbtool, QStringList() << "-s" << game);

    if (!blorbProcess->waitForStarted())
    {
        qDebug() << "BlolbTool start failed";
        return; //"Error"
    }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
    else qDebug() << "BlolbTool started";

    blorbProcess->waitForFinished();
    delete blorbProcess;

    return;
}

//Send the answer from Blorblib to stdout
QString GelekBackend::readBlorbOut()
{
    QByteArray bytes = blorbProcess->readAllStandardOutput();
    QString output = QString::fromUtf8(bytes);

    if (debuggingMode) {
        qDebug() << "--- Blorb stdout output\n" << output;
        qDebug() << "\n-----------------------";
    }

    return output;
}

using namespace std;

void GelekBackend::addToList(const QString savefile, const QString game, const QString dateTime) {
    QDir appDir;
    appDir.cd(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");

    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {return;}
    QTextStream toList(&saveList);
    toList << '"' << savefile.toUtf8().data() << "\": { game:\"" << game.toUtf8().data()
           << "\", time:\"" << dateTime.toUtf8().data() << '"' << " }" << endl;
    saveList.close();
}

QString GelekBackend::whichGame(const QString savefile) {
    QDir appDir;
    appDir.cd(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::ReadOnly | QIODevice::Text)) {return "";}
    QTextStream fromList(&saveList);
    QString searchString(savefile);
    QString line;
    QString game;
    line = fromList.readLine();

    if (debuggingMode) {
        qDebug() << "line: -----------" + line;
        qDebug() << "--------- line: " << line << " isNull? " << line.isNull();
    }

    while (!line.isNull()) {
        if (debuggingMode) {
            qDebug() << "--------- while";
            qDebug() << "--------- line contains: " << line.contains(searchString, Qt::CaseSensitive);
            qDebug() << "---------  searchString: " << searchString;
        }

        if (line.contains(searchString, Qt::CaseSensitive)) {
            line.remove(0,savefile.size()+12);
            QString gameEnd;
            gameEnd = '"';
            QStringRef y = line.midRef(0, line.indexOf(gameEnd));
            game.append(y);
            saveList.close();
            return game;
        }

        line = fromList.readLine();
    }

    saveList.close();
    game.append("Unkown Game");
    return game;
}

void GelekBackend::whatTime(const QString savefile, QString dateTime) {
    QDir appDir;
    appDir.cd(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::ReadOnly | QIODevice::Text)) {return;}
    QTextStream fromList(&saveList);
    QString searchString(savefile);
    QString line;

    while (!line.isNull()) {
        line = fromList.readLine();
        if (line.contains(searchString, Qt::CaseSensitive)) {
            QString timeSearch = "time:";
            line.remove(0,line.indexOf(timeSearch) + 6);
            line.chop(1);
            dateTime.append(line);
            saveList.close();
            return;
        }
    }

    saveList.close();
}

void GelekBackend::deleteSave(const QString savefile) {

    QDir appDir;
    appDir.cd(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");

    QString renameFrom;
    QString renameTo;

    renameFrom.append(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    renameFrom.append("gelekSaveList");

    renameTo.append(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    renameTo.append("gelekSaveListOLD");

    if (debuggingMode) {
        qDebug() << renameFrom << "  " << renameTo;
    }

    rename(renameFrom.toUtf8(), renameTo.toUtf8());

    QFile saveListOLD(appDir.filePath("gelekSaveListOLD"));
    if (!saveListOLD.open(QIODevice::ReadOnly | QIODevice::Text)) {return;}
    QTextStream fromList(&saveListOLD);

    QFile saveList(appDir.filePath("gelekSaveList"));
    if (!saveList.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {qDebug() << "Couldn't open gelekSavedList"; return;}
    QTextStream toList(&saveList);

    QString line;
    //QString saveListContent;
    line = fromList.readLine();
    
    if (debuggingMode) {
        qDebug() << "Is Line Null? " << line.isNull();
    }

    while (!line.isNull()) {
        if (debuggingMode) {
            qDebug() << "Line: " << line;
        }

        if (!(line.contains(savefile, Qt::CaseSensitive))) {
            toList << line << endl;
        }
        line = fromList.readLine();
    }

    saveList.close();
    saveListOLD.close();
}

QStringList GelekBackend::exportSave() {
    QStringList exportedFile;

    QDir appDir;
    appDir.cd(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");

    QString savedGamesList;

    savedGamesList.append(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    savedGamesList.append("gelekSaveList");

    QFile saveList(appDir.filePath("gelekSaveList"));

    if (!saveList.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Couldn't open gelekSavedList";
        return {};
    }

    QTextStream readList(&saveList);
    QString line;
    line = readList.readLine();

    if (debuggingMode) {
        qDebug() << "Is Line Null? " << line.isNull();
    }

    while (!line.isNull()) {
        if (debuggingMode) {
            qDebug() << "Line: " << line;
        }

        exportedFile << line;
        line = readList.readLine();
    }

    saveList.close();

    return exportedFile;
}

QString GelekBackend::standardPathTo(int path) {
    switch(path)
    {
        case 1 :  return QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
                  break;
        case 2 :  return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
                  break;
        case 3 :  return QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
                  break;
    }
}

void GelekBackend::firtRunFolderSetUp() {
    //Create a game folder
    QDir initFolders;

    //TODO: This returns boolean if folders created successfully. Use that
    initFolders.mkpath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/SavedGames/");
    initFolders.mkpath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/Games/");
    initFolders.mkpath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/GameFiles/");
}

//SIGNALS

//Emit the signal for the std from GLK to be read
void GelekBackend::processOutput() {
    //If you need to do json manipulation, this is a good place to do it
    emit stdoutLevel();
}

//Emit the signal for the outstd to be read
void GelekBackend::babelOutput()
{
    emit stdoutBabel();
}

//Emit the signal for the outstd to be read
void GelekBackend::blorbOutput()
{
    emit stdoutBlorb();
}
