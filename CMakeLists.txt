cmake_minimum_required(VERSION 3.0.0)
project(gelek-vanilla C CXX)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

find_package(Qt5Core)
find_package(Qt5Qml)
find_package(Qt5Quick)

# Automatically create moc files
set(CMAKE_AUTOMOC ON)

# Components PATH
execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_MULTIARCH
    OUTPUT_VARIABLE ARCH_TRIPLET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

set(QT_IMPORTS_DIR "lib/${ARCH_TRIPLET}")

set(APP_NAME          "Gelek Vanilla")
set(PROJECT_NAME      "gelek-vanilla")
set(FULL_PROJECT_NAME "gelek-vanilla.cibersheep")
set(CMAKE_INSTALL_PREFIX /)
set(DATA_DIR /)
set(DESKTOP_FILE_NAME ${PROJECT_NAME}.desktop)
set(TERPS_DIR         "terps")

#Let's use a save fix for tads until upstream fix comes in
set(TADS_SAVEFIX      OFF)

set(APP_VERSION       "1.9.1-test")

# This command figures out the target architecture for use in the manifest file
execute_process(
    COMMAND dpkg-architecture -qDEB_HOST_ARCH
    OUTPUT_VARIABLE CLICK_ARCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

configure_file(manifest.json.in ${CMAKE_CURRENT_BINARY_DIR}/manifest.json)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/manifest.json DESTINATION ${CMAKE_INSTALL_PREFIX})
install(FILES ${PROJECT_NAME}.apparmor DESTINATION ${DATA_DIR})
install(DIRECTORY qml DESTINATION ${DATA_DIR})
install(DIRECTORY assets DESTINATION ${DATA_DIR})

#Intall the interpreters
file(MAKE_DIRECTORY ${TERPS_DIR})
install(FILES build/${ARCH_TRIPLET}/terps/build/level9 DESTINATION ${TERPS_DIR}
    RENAME level9remglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
install(FILES build/${ARCH_TRIPLET}/terps/build/bocfel DESTINATION ${TERPS_DIR}
    RENAME bocfelremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
install(FILES build/${ARCH_TRIPLET}/terps/build/magnetic DESTINATION ${TERPS_DIR}
    RENAME magneticremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
install(FILES build/${ARCH_TRIPLET}/terps/build/glulxe DESTINATION ${TERPS_DIR}
    RENAME glulxeremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
install(FILES build/${ARCH_TRIPLET}/terps/build/agility DESTINATION ${TERPS_DIR}
    RENAME agilityremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)

if(TADS_SAVEFIX)
    install(FILES build/${ARCH_TRIPLET}/tads-savefix/build/tads/tadsr DESTINATION ${TERPS_DIR}
        RENAME tadsrremglk
        PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
        GROUP_EXECUTE GROUP_READ)
else()
    install(FILES build/${ARCH_TRIPLET}/terps/build/tads/tadsr DESTINATION ${TERPS_DIR}
        RENAME tadsrremglk
        PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
        GROUP_EXECUTE GROUP_READ)
endif()

install(FILES build/${ARCH_TRIPLET}/terps/build/advsys DESTINATION ${TERPS_DIR}
    RENAME advsysremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
install(FILES build/${ARCH_TRIPLET}/terps/build/hugo DESTINATION ${TERPS_DIR}
    RENAME hugoremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
install(FILES build/${ARCH_TRIPLET}/terps/build/scott DESTINATION ${TERPS_DIR}
    RENAME scottremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
#Adrift interpreter
install(FILES build/${ARCH_TRIPLET}/terps/build/scare DESTINATION ${TERPS_DIR}
    RENAME scareremglk
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
#Copy babel tool
install(FILES build/${ARCH_TRIPLET}/babel/babel DESTINATION ${TERPS_DIR}
    RENAME babel-arm
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)
#Copy blorb utility
install(FILES build/${ARCH_TRIPLET}/blorblib/blorbscan DESTINATION ${TERPS_DIR}
    RENAME blorbscan-arm
    PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ
    GROUP_EXECUTE GROUP_READ)

# Set up About page with version number
configure_file(About.qml.in ${CMAKE_CURRENT_BINARY_DIR}/About.qml)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/About.qml DESTINATION ${DATA_DIR}qml)

# Translations
file(GLOB_RECURSE I18N_SRC_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/po qml/*.qml qml/*.js)
list(APPEND I18N_SRC_FILES ${DESKTOP_FILE_NAME}.in.h)

find_program(INTLTOOL_MERGE intltool-merge)
if(NOT INTLTOOL_MERGE)
    message(FATAL_ERROR "Could not find intltool-merge, please install the intltool package")
endif()
find_program(INTLTOOL_EXTRACT intltool-extract)
if(NOT INTLTOOL_EXTRACT)
    message(FATAL_ERROR "Could not find intltool-extract, please install the intltool package")
endif()

add_custom_target(${DESKTOP_FILE_NAME} ALL
    COMMENT "Merging translations into ${DESKTOP_FILE_NAME}..."
    COMMAND LC_ALL=C ${INTLTOOL_MERGE} -d -u ${CMAKE_SOURCE_DIR}/po ${CMAKE_SOURCE_DIR}/${DESKTOP_FILE_NAME}.in ${DESKTOP_FILE_NAME}
    COMMAND sed -i 's/${PROJECT_NAME}-//g' ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME}
)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${DESKTOP_FILE_NAME} DESTINATION ${DATA_DIR})

add_subdirectory(po)
add_subdirectory(plugins)

# Make source files visible in qtcreator
file(GLOB_RECURSE PROJECT_SRC_FILES
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    qml/*.qml
    qml/*.js
    *.json
    *.json.in
    *.apparmor
    *.desktop.in
)

add_custom_target(${PROJECT_NAME}_FILES ALL SOURCES ${PROJECT_SRC_FILES})
