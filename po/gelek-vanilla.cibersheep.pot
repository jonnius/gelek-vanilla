# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gelek-vanilla.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gelek-vanilla.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-02-24 15:19+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/SettingsPage.qml:14 ../qml/components/GelekHeader.qml:24
msgid "Settings"
msgstr ""

#: ../qml/SettingsPage.qml:38
msgid "Convergence Settings"
msgstr ""

#: ../qml/SettingsPage.qml:55
msgid "Convergence"
msgstr ""

#: ../qml/SettingsPage.qml:72
msgid "Pictures to the right"
msgstr ""

#: ../qml/SettingsPage.qml:96
msgid "Show menu to the right"
msgstr ""

#: ../qml/SettingsPage.qml:116
msgid "Game Settings"
msgstr ""

#: ../qml/SettingsPage.qml:132
msgid "Show ingame pictures"
msgstr ""

#: ../qml/SettingsPage.qml:149
msgid "Don't force focus on input"
msgstr ""

#: ../qml/SettingsPage.qml:163
msgid "Development Settings"
msgstr ""

#: ../qml/SettingsPage.qml:178
msgid "Enable debug logs"
msgstr ""

#: ../qml/components/BottomEdgeMenuL9.qml:11
msgid "Actions"
msgstr ""

#: ../qml/components/BottomEdgeMenuL9.qml:37
msgid "Common Actions"
msgstr ""

#: ../qml/components/SavePopup.qml:13
msgid "Save Game"
msgstr ""

#: ../qml/components/SavePopup.qml:14
msgid "Enter a name to save the game"
msgstr ""

#: ../qml/components/SavePopup.qml:20
msgid "File name already exists!"
msgstr ""

#: ../qml/components/SavePopup.qml:31 ../qml/savedGamesPage.qml:114
msgid "Save"
msgstr ""

#: ../qml/components/SavePopup.qml:63 ../qml/components/GameInfoPopup.qml:124
#: ../qml/components/RestorePopup.qml:62
msgid "Cancel"
msgstr ""

#: ../qml/components/GameInfoPopup.qml:13
msgid "Game Info"
msgstr ""

#: ../qml/components/GameInfoPopup.qml:85
msgid "Ifiction file doesn't exist!"
msgstr ""

#: ../qml/components/GameInfoPopup.qml:105
msgid "Download"
msgstr ""

#: ../qml/components/GameInfoPopup.qml:106
msgid "Re-download"
msgstr ""

#: ../qml/components/GameInfoPopup.qml:125
msgid "Close"
msgstr ""

#: ../qml/components/RestorePopup.qml:12
msgid "Restore Game"
msgstr ""

#: ../qml/components/RestorePopup.qml:13
msgid "There's no saved games yet"
msgstr ""

#: ../qml/components/RestorePopup.qml:13
msgid "Choose a saved game to restore"
msgstr ""

#: ../qml/components/GameListView.qml:19 ../qml/savedGamesPage.qml:87
msgid "Delete"
msgstr ""

#: ../qml/components/GameListView.qml:37
msgid "Info"
msgstr ""

#: ../qml/components/GelekHeader.qml:13
#: ../qml/components/GelekHeaderORIGINAL.qml:18
msgid "Information"
msgstr ""

#: ../qml/InstallPage.qml:34
msgid "Save with"
msgstr ""

#: ../qml/playLevel9.qml:36
msgid "Level 9 interpreter"
msgstr ""

#: ../qml/playGlulx.qml:46
msgid "Glulx Interpreter"
msgstr ""

#: ../qml/SharePage.qml:34
msgid "Share with"
msgstr ""

#: ../qml/savedGamesPage.qml:15 ../qml/Main.qml:246
msgid "Saved Games"
msgstr ""

#: ../qml/savedGamesPage.qml:101
msgid "Share"
msgstr ""

#: ../qml/savedGamesPage.qml:165
msgid "Unknown"
msgstr ""

#: ../qml/ImportPage.qml:37
msgid "Choose"
msgstr ""

#: ../qml/Main.qml:99 gelek-vanilla.desktop.in.h:1
msgid "Gelek Vanilla"
msgstr ""

#: ../qml/Main.qml:137
msgid "Import"
msgstr ""

#: ../qml/Main.qml:146
msgid "a new game"
msgstr ""

#: ../qml/Main.qml:150
msgid "Accepted formats: %1"
msgstr ""

#: ../qml/Main.qml:192
msgid "No Imported Games Yet"
msgstr ""

#: ../qml/Main.qml:192
msgid "Previouly Imported Games"
msgstr ""

#: ../qml/playMagnetic.qml:46
msgid "Magnetic Scrolls interpreter"
msgstr ""

#: ../qml/PlayHugo.qml:45
msgid "Hugo interpreter"
msgstr ""

#: ../qml/playTads2.qml:47
msgid "TADS Interpreter"
msgstr ""

#: ../qml/js/gameFormatHelper.js:4
msgid "AdvSys interpreter"
msgstr ""

#: ../qml/js/gameFormatHelper.js:7
msgid "Adrift interpreter"
msgstr ""

#: ../qml/js/gameFormatHelper.js:10 ../qml/js/gameFormatHelper.js:41
msgid "Agility interpreter"
msgstr ""

#: ../qml/js/gameFormatHelper.js:18
msgid "Bocfel interpreter"
msgstr ""

#: ../qml/js/gameFormatHelper.js:45
msgid "Scott Adams interpreter"
msgstr ""

#: gelek-vanilla.desktop.in.h:2
msgid ""
"interactive fiction;glk;if;text adventure;multi-system;retro;interpreter;"
"game;"
msgstr ""
