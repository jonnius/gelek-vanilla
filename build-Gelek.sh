#!/bin/bash

#In the latest release Alan2 terp doesn't build. So you can `clickable build-libs terps` and select whitch ones you need
#Build libraries
echo "Building libraries\n---------------"
echo "\nremglk:\n-arm\n"
clickable build-libs remglk
echo "⚫ arm64\n"
clickable build-libs remglk -a arm64
echo "amd64\n"
clickable build-libs remglk -a amd64

echo "\babel:\n-arm\n"
clickable build-libs babel
echo "⚫ arm64\n"
clickable build-libs babel -a arm64
echo "amd64\n"
clickable build-libs babel -a amd64

echo "\blorblib:\n-arm\n"
clickable build-libs blorblib
echo "⚫ arm64\n"
clickable build-libs blorblib -a arm64
echo "amd64\n"
clickable build-libs blorblib -a amd64

echo "\nCoying remglk lib into terps folder:"
echo "⚫ Creating folders\n"
mkdir -p build/arm-linux-gnueabihf/terps/remglk \
build/aarch64-linux-gnu/terps/remglk \
build/x86_64-linux-gnu/terps/remglk

echo "⚫ Copying remlgk to terps/remglk folder\n"
cp build/arm-linux-gnueabihf/remglk/* build/arm-linux-gnueabihf/terps/remglk 
cp build/aarch64-linux-gnu/remglk/*   build/aarch64-linux-gnu/terps/remglk 
cp build/x86_64-linux-gnu/remglk/*    build/x86_64-linux-gnu/terps/remglk

echo "⚫ Building terps"
echo " * Warning Alan2 terp is disabled for now"
echo "⚫ arm\n"
clickable build-libs terps 
echo "⚫ arm64\n"
clickable build-libs terps -a arm64
echo "⚫ amd64\n"
clickable build-libs terps -a amd64

#build project for desktop
echo "Building project and run for desktop"
clickable desktop
