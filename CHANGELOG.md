v1.9.0
- Updated remGLK
- Updated several interpreters
- Code and layout cleaning
- Fixed headers and overlapping images on Level 9 games
- Better game detection
- Fixed ifiction files not updatable
- Updated About page

v1.8.1
- Updated French translation (thanks Anne Onyme 017)
- Quick fix for saving/restoring TADS games

v1.8.0
- fixed not able to save under certain circumstances (terp giving error when cancelling)
- better error handling
- fixed terp stopping when pressed Esc to dismiss save/restore Dialog
- fixed timer being overlooked (fixes playing «Ke rulen los petas»)
- fixed inline images being ignored
- unified all only-text terps under one qml page
- added missing scott terp
- added desktop keywords
- updated Catalan translation
- fixed SuruDark errors
- cleaned codes
- deleted unused qml files
- updated About page

v1.7.1
- Fix saved games list not showing savedgames

v1.7.0
- Updated project structure
- Fixed issues with interpreter chooser
- Added advsys and adrift interpreters
- Fixed image size
- Fixed images not showing in glulx
- Build for arm64

v1.6.7
- Update App Name in desktop File

v1.6.6
 - Fixes games not working on fresh app install (thanks Katherine)
- Fixes first game imported not showing in the list
- Fixed About typo
- Updated translations
- Qml clean up and small changes to code

v1.6.5
 - Added text styles
 - Fixed preformatted text line break
 - Empty images don't show empty space
 - Added ScottAddams and Agility interpreters
 - No automatic scroll
 - No debug info text

v1.5.5
 - Fixed saving issue when using TADS games

v1.5.1
 - Fixed saving issue on L9 games
 - Fixed missed saved game icon
 - Fixed zblorb not recognized correctly
 - Other minor bugs

v1.5.0:
 - Added support for TADS, Magnetic Scroll and Glulxe interprets
 - Updated Catalan translation
 - Fixed bug stopping games if action was entered while drawing an image

v1.2.1
 - Changed interpreter fortz with bocfel
 - Added changes for Level 9 games from Gelek
 - better image size
 - mini menu for small screens
 - little bug fixes
 - Initial support for z6 games and grid text
 - trunkated json bug fixed
 - Support for Utf8 characters (non English games)

v1.1
 - Bug fixes

v1.0
 - Initial commit
